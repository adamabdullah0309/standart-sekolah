<?php
// $content = preg_replace("/<img[^>]+\>/i", '', $kop); 
// $doc = new DOMDocument();
// $doc->loadHTML($kop);
// $xpath = new DOMXPath($doc);
// $src = $xpath->evaluate("string(//img/@src)"); # "/images/image.jpg"

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Laporan Kunjungan </title>
    <style>
        /* table {
            page-break-inside:auto;
        }
        tr, th {
            page-break-inside:avoid; 
            page-break-after:auto;
        }
		body{
			width : 21cm;
            font-family: 'Proxima Nova',sans-serif;
		}
		table, tr, td{
			border : none;
		}
		td{
			height : 16px;
		} */
        .bordered_table,
        .bordered_table tr,
        .bordered_table td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        .fr {
            /* float: left; */
        }

        .fl {
            /* float: left; */
        }

        .txt-center {
            /* text-align: center; */
        }

        .txt-left {
            text-align: left;
        }

        tr.tbl-txt1 td {
            /* padding: .1rem .2rem; */
            font-size: 12.5px;
        }

        tr.tbl-txt2 th {
            /* padding: .2rem; */
            font-size: 14px;
        }

        tr.tbl-txt3 th {
            /* padding: .2rem; */
            background-color: #ddd;
        }

        tr.tbl-txt4 td {
            /* padding: .2rem; */
            font-size: 14px;
        }

        .tbl-txt5 {
            /* border-top: 2px solid #000; */
            /* text-align: left; */
        }

        .tbl-txt6 {
            /* border-top: double; */

        }

        .tbl-txt7 {
            /* text-align: right; */
        }

        tr.tbl-space td {
            /* border-right: hidden;
            border-left: hidden; */
        }

        hr {
            /* border-top: 3px double #000; */
        }

        .footer .page-number:after {
            content: counter(page);
        }

        #number_page:after {
            content: counter(page);
        }

        @page {
            margin: 100px 25px;
        }

        header {
            position: fixed;
            top: -90px;
            left: 0px;
            right: 0px;
            height: 20px;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;
        }

        p {
            page-break-after: always;
        }

        p:last-child {
            page-break-after: never;
        }

        /* @media print {
			.header, .hide { visibility: hidden }
		} */
    </style>

    


</head>

<body>

    <table align='center'>
        <tr width="100%">
            <?php
            $path = base_url('assets/img/profile/default.jpg');
            ?>
            <th rowspan="4"><img src="<?= $path; ?>" width="80" height="80"></th>

        </tr>
        <tr width="100%">
            <th colspan="7">MI Islamiyah Madiun</th>
        </tr>
        <tr width="100%">
            <th colspan="7">Jl. Hayam Wuruk No.14 A, Manguharjo, Kec. Manguharjo, Kota Madiun, Jawa Timur 63127</th>
        </tr>
        <tr width="100%">
            <th colspan="7">(0351) 493344</th>
        </tr>
    </table>
    <br>
    <table width="100%" class="bordered_table">
        <tr width="100%" class="tbl-txt2">
            <th>Nomor</th>
            <th>Name</th>
            <th>Email</th>
            <th>Photo</th>
            <th>Jenis Kelamin</th>
            <th>Alamat</th>
            <th>Tanggal Lahir</th>
            <th>Nomor Hp</th>
        </tr>
        <?php
        $n = 1;
        foreach ($list as $row) {
        ?>
            <tr class="tbl-txt1">
                <td><?php echo $n; ?></td>
                <td><?php echo $row['name']; ?></td>
                <td><?php echo $row['email']; ?></td>
                <td style=" background-color: black;"><img src="<?= base_url('assets/img/profile/') . $row['image']; ?>" width="80" height="80" alt=""></td>
                <td><?php if ($row['jenis_kelamin']  == "l") {
                        echo 'Laki - laki';
                    } else if ($row['jenis_kelamin']  == "p") {
                        echo 'Perempuan';
                    }; ?></td>
                <td><?php echo $row['alamat']; ?></td>
                <td><?php echo $row['tanggal_lahir']; ?></td>
                <td><?php echo $row['nomor_hp']; ?></td>

            </tr>
        <?php
            $n++;
        }
        ?>
    </table>