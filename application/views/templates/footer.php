<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Sekolah <?= date('Y') ?></span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?= base_url('auth/logout') ?>">Logout</a>
            </div>
        </div>
    </div>
</div>

Bootstrap core JavaScript
<script src="<?= base_url('assets/'); ?>vendor/jquery/jquery.min.js"></script>


Core plugin JavaScript
<script src="<?= base_url('assets/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

Custom scripts for all pages
<script src="<?= base_url('assets/'); ?>js/sb-admin-2.min.js"></script>

<script src="<?= base_url('assets/'); ?>vendor/datatables/jquery.dataTables.min.js"></script>
<!-- <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script> -->



<script src="<?= base_url('assets/'); ?>vendor/datatables/dataTables.bootstrap4.min.js"></script>



<!-- <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script> -->

<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<!-- <script src="<?= base_url('assets/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script> -->
<!-- <script src="<?= base_url('assets/'); ?>js/sb-admin-2.min.js"></script> -->
<!-- <script src="<?= base_url('assets/'); ?>vendor/datatables/jquery.dataTables.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap5.min.js"></script> -->

<!-- <script src="https://unpkg.com/@popperjs/core@2"></script> -->

<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/image_helper.js"></script>
<script src="<?= base_url('assets/'); ?>js/print.min.js"></script>

<!-- <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<!-- <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>









<!-- <script src="<?= base_url('assets/'); ?>js/demo/datatables-demo.js"></script> -->

<script>
    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });

    $('.form-check-input').on('click', function() {
        const menu_id = $(this).data('menu');
        const role_id = $(this).data('role');

        $.ajax({
            url: "<?= base_url('admin/changeaccess'); ?>",
            type: "post",
            data: {
                menuId: menu_id,
                roleId: role_id
            },
            success: function() {
                document.location.href = "<?= base_url('admin/roleaccess/'); ?>" + role_id;
            }
        })
    });
    // $('#dataTable').DataTable({
    //     dom: 'Bfrtip',
    //     buttons: [{
    //         text: 'Add new button',
    //         action: function(e, dt, node, config) {
    //             // dt.button().add(1, {
    //             //     text: 'Button ' + (counter++),
    //             //     action: function() {
    //             //         this.remove();
    //             //     }
    //             // });
    //         }
    //     }]
    // });

    // $('#dataTable').DataTable({
    //     buttons: [
    //         'copy', 'excel', 'pdf'
    //     ]
    // });

    // var table = $('#dataTable').DataTable();
    // $('#dataTable').DataTable();

    $(document).ready(function() {
        function getBase64Image(img) {
            var canvas = document.createElement("canvas");
            canvas.width = img.width;
            canvas.height = img.height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);
            return canvas.toDataURL("image/png");
        }
        $('#dataTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy',
                {
                    extend: 'excelHtml5',
                    text: 'Excel',
                    exportOptions: {
                        stripHtml: false
                    },
                    disable: true,
                    action: function(doc) {
                        $.ajax({
                            url: "<?= base_url('admin/cetak_excel_list_siswa'); ?>",
                            type: "post",
                            // data: {
                            //     menuId: menu_id,
                            //     roleId: role_id
                            // },
                            success: function() {
                                // document.location.href = "<?= base_url('admin/roleaccess/'); ?>" + role_id;
                                window.open("<?= base_url('assets/print/list_siswa_excel.xls'); ?>", 'Laporan Siswa', 'height=600, width=800, resizable=1, scrollbars=1, menubar=0');

                            }
                        })
                    }
                },
                {
                    extend: 'pdfHtml5',
                    customize: function(doc) {
                        for (var i = 1; i < doc.content[1].table.body.length; i++) {

                            var tmptext = doc.content[1].table.body[i][3].text;
                            tmptext = tmptext.substring(10, tmptext.indexOf("width=") - 2);

                            doc.content[1].table.body[i][3] = {
                                margin: [0, 0, 0, 12],
                                alignment: 'center',
                                image: tmptext,
                                width: 30,
                                height: 30
                            };
                        }
                    },
                    exportOptions: {
                        stripHtml: false
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        stripHtml: false
                    },
                    customize: function(win) {
                        console.log(win.document)
                        $(win.document.body).children('h1').text(' ')
                        var gambar = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTERUUExMVFhUXGCAbGRgYGR4aHhshHSEaGhkiHiAfHiggHh4mIRsdITEiJSkrLi4wHx8zODMtNygtLisBCgoKDg0OGxAQGysmICYrLy0zLS01LS0vMistLy01LS8wLS0tMTItKy8tLi0tLy0tLS0tMi0tLS0tLS8tKy0tLf/AABEIAOQA3gMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAABgMEBQcCAQj/xABTEAACAQMBAwYGDggDBQcFAAABAgMABBEhBRIxBhMiQVFhBxQycYGRFiM1QkVVdHWTobPD0dIzUmJygpKxshUkQ1NjosHwFyU0RHODwghko9Ph/8QAGgEAAgMBAQAAAAAAAAAAAAAAAAQCAwUBBv/EADIRAAIBAgMFBwQCAgMAAAAAAAABAgMRBCExEkFR0fAiYXGBkaHBBRMysRThQmKisvH/2gAMAwEAAhEDEQA/AOpcsuUwsIon5l5mmmWFEQgEs4YrqdPe49IrK9mF98TXX0sX5qh8K3wX862/3lPVACX7ML74muvpYvzUezC++Jrr6WL81OlFACX7ML74muvpYvzUezC++Jrr6WL81OlFACX7ML74muvpYvzUezC++Jrr6WL81OlFACX7ML74muvpYvzUezC++Jrr6WL81OlFACX7ML74muvpYvzUezC++Jrr6WL81OlFACX7ML74muvpYvzUezC++Jrr6WL81N9xcpGMu6oO1iAPrrnnLPlzuXVuLWZJIoDzt2Y2DDcZhGFJBxkKZJMfsL2iuNpZsErmp7ML74muvpYvzUezC++Jrr6WL81NFjta3mxzM8UmeG46tn1E1croCX7ML74muvpYvzUezC++Jrr6WL81OlFACX7ML74muvpYvzUezC++Jrr6WL81OlFACX7ML74muvpYvzUezC++Jrr6WL81OlFACX7ML74muvpYvzUezC++Jrr6WL81OlFACX7ML74muvpYvzUezC++Jrr6WL81OlFACJe8uruKN5ZNkXKoilmYyRaBRknj1CnDZF+J7eKdQQssayAHiA6hgDjTIzWX4QPcu++TS/2NUvIn3NsvksP2a0AL/hW+C/nW3+8p6pF8K3wX862/3lPVABRRRQAUUUUAFFFFABRRVPa21YbaJpp5FjjXizH1AdZJ6gNTQBcr4zAAk6Aca5vfeEyVgzW9liIAnnrqQQjA6wgDNu9eTjq0rT5HbdvNpWsrTWywROhWKQOx5zeBG8FZQQnAhjjPZUIzjJtRd7HXFrUw9kbITa13cTXy85G1tC9tHkjmY5nmKlccJCsKMW45YrwGKXrK/VOfRVlmWCWRWeOLeKxo5jVpdwYycE6DJHS3cAmnfZHOC3s763j5xltlguYFwGKpoQmcAyRSBxukjIZxxxWjsvlNsmCPcjuLa3GSTG7CFwx1beR8OG7cjNV4jDwrx2ZEoVHB3Rz6w2HDLsxtoRyYvBOFSZMYi3ZxGBGq4BDIc9LJO9gkjSuq8ldptcWyvIAJVLRyheAkiZo5N39kspIz1EUv2lrHdyKttCIbFZhcSvuc2LmVcFNxSASgZVdpMYYqoGQWNUGvdoixe42bFDIZbmaTD5JMRchGjGVDFgu/qeBGAc1dGKirIi3c6NRXG9lcttqOCyT20rKcSQywNCynrVsOSrceIIp45J8uYrt+YlRre6AyYXIO8OsxMNJF82CMHSq4VoTk4p5rdvOuEkrsbKKM0VaRCiiigAooooAKKKKAMDwge5d98ml/sapeRPubZfJYfs1qLwge5d98ml/sapeRPubZfJYfs1oAX/Ct8F/Otv8AeU9Ui+Fb4L+dbf7ynqgAooooAKKKKACvhNeJ51RSzsFUDJZiAAO8nQVzHltyoS/bxKzmDQbu9dTRtkFTkLErD9bB3iPe6dZFQqVI04uUtEdjFydkX9v+FBELrZw+MCPPOTs/NwJjOcPgmQjsUdY1pRTaM99u3u0NxFQb0MQyI4lxkyEEnpntPAY4dVFoFuZhAigWlsQHC8HkGoTHWq8T2nFMGwdj/wCJ3JVhmyt29tPVPKMER96LoW7Tgd9Zkq9XEP7Ucr5vuXNjKhGmtp58yzyS5NHaJW6ulIswcwQMMc9jhJKD7z9VDx4nTj1UChVwMCgnHGtKlSjTioxWQvKTk7sXLqwntZXmtEEsUrb01sWCHePlPCx6IZvfI2FY65U53vjcq4M+2W14rj3ps5nI/iRGU+dWIrxtfwg7Ntzuvdxs+cbkWZWz2ERg49OKwZvCzET7TZXbj9ZgkQPeN5sn1CpuSWrCMJS0VzdufGb72vmpLW1bSR3IWaVetUVSeaVuBdiHxkBRneDLBAqIqIoVVAVVAwAAMAAdQA0pDtvCzZ8JobuDtLxby+uMt/SmTY/LCwusCC7hdjqE3wH/AJGw31UJp6HHFrVGXy25Fi6PjFuwhvUHQkx0ZB/s5QPKQ9vFdCOw86dFu0aKVWguYGwwziSCQcGUj3p4hhowrutJHhD5LPKBeWijxuEarw5+Pi0Z/a61PUdOvRTFYb7i2oZSWj+CylU2cnoc75N7SvomaNL2ZbiLG9HOxnikXqdd7pBW6905Brp3I3lmt2zQTJzF2gy0ecq6/rxN75e0cV4Htrmt7GLqGO5tmxMnSjJ6+p437jqCOo17WQXcMc8DGKeNt6NuDRSLoyt3e9YEYI6qSpY+cXeel7PjF8i+VFNdnX9ndKK5dsHwtkqTe2bwqrFJJYjzqxsOPOIBvoOvI3hqK6XaXSSoskbq6MMqynIIPAgjjWwpJ6MUaaJqKKK6cCiiigDA8IHuXffJpf7GqXkT7m2XyWH7Nai8IHuXffJpf7GqXkT7m2XyWH7NaAF/wrfBfzrb/eU9Ui+Fb4L+dbf7ynqgAooooAKyuU23Y7K2e4lyQuAFXVnYnCKo6yTp9fAVjcuuV5tNyC3QS3coJRD5KKNDJJjUID1cWOgrl0YuLm/JuLqWcW4DMD0YhKwO6EjA3RuKc545IpeviIUk76pXt13lkKbloS8pbiWWNrraJ51h+htQTzSM2iKFHluc6sc9fUKrJC1napDHjxq4Y8MY321dv3Yx3dQ7atf+Jvf9zaeppSP/AIL6ia+bKkEjy30hxGAUizwEa6s/8ZBPmArFnVnJXqZ72u9/jH5Y2opfj1xZMtkyLBYWhPPTdEORncUayyt38T3kjFdMk2js/Y1rFC8qxIi4RPKd9dSFUbzEsckgYyequMbF2zes808QW3M2FEzDekWIYKpGp6K5OSzHOSQRjFT22z0Vi53nlbypZGLu3Vqx16qeo1IYaFnnJ5vxJRwtSu76RG/anhPuptLO2EKf7W58o+aJTp/EfRSntBJrnW8uZrjh0C25HprpGmF/rViobm7jjGXdVH7RAqqeKqzyWXgP08FRpq7z8QtrZIxhEVR+yAP6VNWV/jsbfo0ll/8ATjJHrOB9dev8Qm6rSX0sg/8AlXFha889lk3i8PDLaRp1Vu9nRSfpI0bvI19fGqp2sy+XbTr3hQ4H8pz9VS2u2IJDhZBvfqt0W9TYNRlRrU82miUa9CrkmmXtm3d5a48VvJUUf6cp56PzAPqv8JFN2yfCo6YW/tio657fMid5ZD01Hm3qUqKnTxlSOuZTVwFKemT7ja27zEU4vbSRJLK7fEpjORFOeDHHkiTgwIGGxnjWRf8A+UuefGkExCzjqR+CSdwPkt6DrWVf7HRw+4zRNIu65Q4DjsdeDDOuvX11q7An8YgktrjpSRjm5P21I6Djzjr7QaqxMoSf3YrLSS7uPXcKOhOl2X5M+7XHi04ugPa3xHcDu4JIf3Sd09xrW2Bt5NkT4kYrs+cnIClhBJxBUKCdx8HKgHBxwrL2Gd6OWzn6TRDcbP8AqRsOg3pGh7xUGzbfnYJrGcktF0A3WUOsLjvGB6VqFGtKjK7emT74vR+X6sVTgpq3H9ncdk7WguYxJbypKn6yMD6D2HuNXa4DyekmUeMW5EV7C3NzLwSYp1SDQEONQ/EZ0Olde5L8r7a9jQpIiykdOBnHOIw0ZSvHQg64141tUa8at0tVqutwnODiMFFFFXkDA8IHuXffJpf7GqXkT7m2XyWH7Nai8IHuXffJpf7GqXkT7m2XyWH7NaAF/wAK3wX862/3lPVIvhW+C/nW3+8p6oAK57yw5dSrLJb2QQGEe33MgJjiON7dVdN9wNTrhdM56m7lRtlbO0muW4RoSB+s3BF/iYhfTXD7y2cW8NqzEzXUhaZus59tuDoP4fSKVxVd00ktX7Le/Itpw2tSXZ+0JFtpdoXbmWeVQxZgFJUdGFQF6K50OB1tXuImxsSzDenY7x7XmkPDv1IHmFSbUUTXUFuPIi9vkA4dHoxL/Nk47BXy4/zF+qcY7Uc43fI36MfwrlvOaxZT23tS39p+C/FdcUOJWVl4ee8q3Nm0VtDZIx524JEj9eD0p3+vdGe0UcpGDGKyjGIwoaUDqRdET+Ij1CrGy7hWae+c+1gFIz2Rx53mH77An0Cse0uAkb3M5CtMd9s9Q4RqOs4XGnHjU4J7V3qv+z5L3LKUFJ56fC5/o1ao3u1URtwZkkPCNBlvT1KO81ABPcfrQQ9v+q4/+A+utKyso4l3Y1Cjr7T3k8SfPWrhvpcp9qpkuBzE/VYx7NLPvM4WtzL+kcQKfeR9J/S50B/dFWbXYkEZ3hGGb9Z+m3rbP1VoUVtUsNSpK0UYtXEVKrvN3I5pgoyc6kAAAksToAoGpYnQAcaNi7CubvaEluZBA8NtzoQHfCuzoFScDiShyQpyuRgkgg1b/bYsrZ7tcG6eRoLTIyIVUKJ5QDpvktuAnhp1FgWj/wCnfZLiC5vZCWaeTdBY5JCZLMTxJZmI1/VqmrWbdkShBJXZiTrJFKYJ4zFMNd06hh+tG3B17xqOBANQ3dlHKMSIr/vDPqPEV2Dl1sRLqylUgCRFLxP1o6glSD6MEdYJFcgsp+cjR8Y30VsecA/86uo1NtWZXOOzmjNOyJI9baYqP9nJl08wPlL6Ca+JtfcIW4QwsdAxOY28zdXmOK2a8yRhgVYAg8QRkH0VRX+n0qudrPuGaGPrUt91wZ4BrPvpTBLHdLnCdGUD30bcfSp6Q9NRvsuSHpWzdHrhc9E/uHih+qprPaCTb0bAq+CHifQ44HzjvFYdbB1MO+0rx+Dbp4qliobOj+TT5Q+1NFepqI+jLjXeicjJ0GTunDD0192+RDJDeL5K+1ykdcbkYb+FsH0moOSsgMctnL0ua6Iz7+J87vq1U+YVLsCMNDLZTamHMRzxaNh7Wf5dP4azmth2eezl4wfL5XAVaenV0er7/L3kcw/R3GIpP3x+hb06p6qp7Q2TCL7EkYKXI6LYwySpr0WGq7y68dStS2EBubGS1kPtsWYi37SYMT9uo3Tnz0Tu93s8SAYnj6YHWJYj0h6cEeZqlFuL1/1b7v8AF9bkcdmvfmjoHg52/Lzj2FzIZJEXfglbypYs7pDnrkQ4BPEgg9pL/XC59qBUtdoxj9CyyHHHm3wk6/yk/wAtdyjcEAg5BGQe3srawdZ1ad5arJ+KEqsNmWWhheED3Lvvk0v9jVLyJ9zbL5LD9mtReED3Lvvk0v8AY1S8ifc2y+Sw/ZrTRWL/AIVvgv51t/vKeqRfCt8F/Otv95T1QBzrwp3vOTWlkDoWNxKM+8i0jBHY0hH8lJtgedvp5T5ECiFezePTlPnGVFX9r7SD320btjlITzK9ywLmTHbmRm9VYEivFs1E4T3JC9+/Ocv6lJ9VYmMl9yq4ruj65t+mQ5SWzFPz5FrYVyBFcX0nCVmcaYPNx5WMecgE/wAVUolkjsR1XN6/foZfrASP1EVe5QW4ItrFPJdhvD/dRAFvNnCip4/br9m95apujs5yQZc/wpgek0spL8/PyjlFev6LbbvLzepkcsZxHHb2USF97GUHEonkgnqDMBk9gavlnsslxLOQ8nvQPIj/AHR2/tHWvezJOeeW5P8AqNux90aZC+s5b01o16b6fglSpRlL8tfNmbiMRKTcVoFFFFaYoFfCcamvtV9oQ78ZGmmG1G8DukOAR1qd3BHYTXHkjqMG45HXe0Zi1vjxVQTz0h3IUJJMmGI6WoySobjT14PNoS2tikUV/Z7gd/KtppMdNgcMkqqVJGRp11T2hymVLvZaXI8aNzHFLIsv6KETYEYhiXEY3Dk7zq7Yx0hVzlhynuLDZTG1fm3faVzHvYBwomuGOAQQNQOrhmsxu7uNJWL3KDlddlRbJdWEslyrxKBHLbFCUfdbfkkZT0sKBjVmUZGaV4xzbCB0eKRFA5uQbrYGmR1Mv7Skjvpl27yoU2trfTRxy7+zJi0bDKO7SWSEEH3u+3DspUt9pNcRWLl23DHORESWETK8cZCM2X3CN1grM27wBxgC2jJqVlvI1FdFyiiinxYKp7R2akwG8CGHkupwynuP/KrlFcaTVmdTazQrz3s1pPDLMN4KdxpVGjxtxDjqZThh1HWmra/tNzBcjyH9pl7MMcxN6G0z2NUU0QZSrDKkYI7Qag2NF4xZzWchy8XteT2cYX9WP5a879UwkaTjVj+Oj8HyNLD15VLqWupcnHMbQRuCXSbjf+pHqhPnXK+iix9pv5YveTrzydm+uFlHnPRaql273WzRJj/MQ4fhqJYT0vScN/NUm37gNBb3qf6TLJw13HAWQeo/VWRst9l98H4r8fhDV9/nzPWwrdR43ZMOgrEqP93MCcDzHeFdQ8Fu0Gm2ZAH/AEkIMD651iJjGe8qFPprmm0/a722mHkyhoGOe3pxfWCPTTh4LZ9y6v7fQAslwnfzi7kn/FGPXT/0+peb/wBlfzWT5lFePZ8HYaPCB7l33yaX+xql5E+5tl8lh+zWovCB7l33yaX+xql5E+5tl8lh+zWtYVF/wrfBfzrb/eU531yIonkbgiFj5lBJ/pSZ4Vvgv51t/vKx/DqJDHaiOV4995I23ScEPGQQwzgggEa9poBuwjOrNs+2iJJe7lUue6RjNIfVpWrdDndoRJ723jMh7N5+gg9ADGl+Ha7LPbNcQlUgRlDRZdSSFUHd8pQFB7eNXdg7dt83lwZE3mckKThjHEuE6J111OO+sGrSqK7tnn6ydv0O06kJWs//ABf2W4LpfGby6bVLdOaX+Ec5L6d4geiqkweHZ6pwuLtukexpuk5/hTI9ArzFanxS0tm1e5fnJe9c89Jn/hX01Yv5Odvj+rbpuj9+TVvUuB6TVmGofdrxgtL+0cl6u5yrPZg5dXZPBEEVVUYVQAB3DQV7oor15jhRRRQAV4m8lvMf6V7qMxvI4hj5oMyOxeaTm40VN0MWbBPvxoBUZNJXZ1K7EzbtyW2lZD/Zw2Sj6KFv6safvCVsuefZpWCGSVl2pcErGjOQC0xyQoJxqNe8VZsuSkcyjm+du5REkay28QtoRuIERjcP05MBVzzTEHGqVpSWkcACyWVgrde9tV0JPWSDENTWYNihtjZ8w2FaRSxyJItpdko6lWAW7tHGVIyOiAfNrWPyOY+L22erxkD12p/qTXRW2O00kEtvaRgRMzObXaJeRlKMu6jdEDpbrakA7gB0NY3KGzWMGVJlzAkhe2uIRa3BDFWkcbiCOdugOkqgH9Y1Om0pJs5JXRBRQaK0hQKKKKACqJl5i7im4JL7TJ6dYm9DaZ7DV6q20rQSxPGffDQ9h4qfQcGqMTRValKD3onTm4SUkXLAczfTRe9nUTL2bwwko856LemodgW4MdzZNwjdkA/3coLJ/Uj0VTuL8va214RiS3kAl01APtU4+sNV7aU6299HM7BYpojG7EgKGTpoSe8FhXjXCWcd9v8AlD+jaTWu74f9mcztJsnJ/S2x168Nbtr61X66beTV6E2raSjybmCSInq4CePP8reukuDlBAk12qBp45iGURLkbzLuygk4HUDn+tYt+J3s1SZlVIIzuqvlEhSoLN5jjdGnbTuHpTVRStZXv5SWnkxarWgo7Leenoz9GcvjnZV8f/tZf7GqbkT7m2XyWH7Naytv2vNbAmjxjcsGXH7sOP8AlWryJ9zbL5LD9mta4uL/AIVvgv51t/vKp+G+I+J28g/07uPPmZZE/qwq54Vvgv51t/vKueFizMuyLoL5SIJR/wC0yyH6lNcZxq6scdqtfW0TriRVIJA1GuToMHiKjuJXyrIyiMrliwzu9YI1HEZ49grEIa6k3Y2bcUjelbtGo3V0UegZ7e9aMd5nwhvvYYdmbR8WnD3Ls8axFInPvdd8q3ax3QA3cBV7YUTCEM/6SQmR/wB5+l9QwPRVO6t1kQowyCMH8fPU/J+8Z0aOQ+2RHdb9oe9b0j6waZwEYKo5b2hj78qkNl7jVooorWIBRUN3cLGjO2d1Rk4qKdrko27Y34JU7p8VkwDjQ8KhKcY6nVFvQyrjaskly0ELqiqpzIV3ssuN4DUDTI+up4rCXfSXxos6ZKExRMBnGSAykZ0GvEVnWEagWJA0dXVu8uu8c+dlNX5IrQCPO7jWKPBJHTyGXTPHXU99efxGLqylk3bgs+PI38Pg6UY9pJvi3bVLmatxtS/dw086XkYGOZmDRRkdeRCyo5xp01Yd1NOyuX+zoYysmzzbSgdGKKFXWQ9kbIAuevpbtJMVnFkCNsNEm4ArZ3c8MjPHszVa7jKvaKzF2DnLEYz0G1xVdPFSvZu/sTq4KFrpW8Hdam7t/bUt42RaWdqPesYlmnHeHwFU8OAOO2si9lkmgEE+0JZYydwK7Rtlh1ZZSd4efIo8XRd1p5A7x7zqx6JA68KOIAqIXVsBkIMBefyIz16bw08r6/VUXXqS0b8kWLC0YLNLzefsVb6SaJUaG6Mm9Kse64RgSTjBYDIra2XfCZN7dKsGKsp96w4jPA+esy8iQPaqihQ0pkwBjUIxJI7dav8AJbZF/Jb87DYvLHI7uriSJQ2WI4M4PV2VpYHEyeU3l3+JmY/DwjnBW8PBcy/RUEEr78kcsRikifcdCVbBwG4qSOBHXU9ayaaujMasFFFfGYAZOgHE104YV7eC3a4iKM63SZRB/tPIfXqyCGJ7qzrmPm4uduGM0iKAN7ULwUBRw9PE8as2bmaRrhuB6MQPUg6/O3H1V52ttNITHvgkEk6DOMdfrNYVRQ+7JwWrJTrTlamtx4ttv27aB93uYY+vh9dWJ1E6pGhyJpUiBHXvOqnB9dZi3NnMXLmPjpvdE4AHXoc5J4d1M/IbZgbaGz4QMqjmY93NqzKT/GV9NCitpEIwW2smdo8IA/7qvvksv9jVNyJ9zbL5LD9mtQ+ED3Lvvksv9jVNyJ9zbL5LD9mtMDov+Fb4L+dbf7ynS8t1kjeNhlXUqw7mBB+o0l+Fb4L+dbf7ynqgD8wQ2pETwSKGeFmiZTwLRkqP6A1jx3LyDdtrbdX9ZiQB26Zx/WuieEfZvi+1XbHtd2glXhjfTCSgd+N1vTSu0zKxRY86bwO8AO/j15/qKXeTaEpdiTXmRbGtZY1KyOHycg66Z4jzcPrr1cyczMk/vfIl/dJ6Lfwn6jVK62y4KIqIzvwUMTgd/RGPN1YNbMiBgQwyCMEefjXFKUJKRBuUZbT3mzUFzOymNUjaR5HCIi4ySQT1kDqNZWwJZ98WiQvPIATGVZAWQY/WIyy5xgdQzTHbbG2itzbS/wCGz4hmEhG/CMgBhge2cda1nXi43TzL4wv4FPavJ/aUsMka7OnBdcAlosf310ubldcwxF32VdKkaFmPOQHAUZJ/SZ4CvG1OW11bxPNLsm4WOMbzMZYNB5g5PqqLbW2doT2s0S7ImBlidATPb4G+pUE+2d9KSm5O7GFFLQ4lazKbeBkBVY7rAB4hWZgAe8BxW+00MSORuqsZywUZ3Se0DUHWsXadnLBFdQyxGGWEQuUJVtQkWTlSQQ24TpwzjiDW/GIwzbu6GbpMBjJ6gSONY+IVn5v4fybuFd4q1tF8r4RTZbaVmHRJVkduKnOMoSdM6dVQX0DLNBl2bemYjOOiCjaDuFWDc28yoG3WEpygYEFincR1d9Ur+3QyxGNz7ZK2WDZwebYdHqGKhDWzvv18CypbZurPNaePXrcuxqibvOusk0aM28QN/d1yQo6uqpU2ipCbqyFWQuGC6ADq7ieoV5LRwhC53m0i5wjeYk9RIGeOtepNpoocsHUI4XJU6k4xu41I141Bq+5vrcWp7O9LrezOvLoNLDJggLBLJhhgjRRqOrrroXg25UXx2bAltsrnY4l5vf8AGo495l8o7rLkamubcoG9suD2WwQeeRyPwrrnJLZe17KFokt7Jg0ryazuMb5zjSLq4ZrSwq7PXj8mPjn27Pv5fAtXXJra0lxcT+IIvPyb+6bmPK9FVxkDXyc+msyVLiK4aC5gEUgjWQYkEmQxZRwGmqmuhbL5RbWuGnWO1sgYJTE+9PJ5QVWOMRajDjs66w9vcktr3V34yUsUPMrFuiaQjCs75zzXHpY9FaFOq4tJvIz5QTF6sXlHOWC26npS+Uf1UHln0+T6TTHtzkxtG1geeZ9npGgyTzkxJ6gAOb1YnQDtpS2fG5LTS/pJMZHUqjyVH9T31KviYqHZ1F5dhXZcRQAABgDQCsg8obfeIJII0OVPVV++kdVygGhBbuUatgdZxVfaFsNWABVv0gxk40yy9+ND3ajUa5kUt5RBL/IpjxOdgECl85yEK4xqSdAO70107wN2HOXt1ckaRIsCHqJY85J6RhB6a58nMorTqoA3eoAZAydNOv8ACu6eC/YhtNmxK4xLLmaXTHSk6WD3qu6v8NW01mM0VeV88uJd8IHuXffJpf7GqXkT7m2XyWH7Nai8IHuXffJpf7GqXkT7m2XyWH7NauGRf8K3wX862/3lPVIvhW+C/nW3+8p6oASvCxsFrmxMkQzPbHnowOLAD2xOGekmdBxIWuOGYvFvxEZIyueB68H+lfpiuCctuT/+H3pVRi1uCXhONEfjJH3a9Je441waqqRuri9eF1tLcJ+w7MIrTykb75Yk6boOvoPb6q92W2eclbdVuZGBv9jfgcjzcTgcDbWz99lLOwhzl16h2HuHb2ce01V2ttOJYuYgwzMN0BNQAdDw4mofl1oU2U89b+xvSo2VZHMcqNvRyLxRhwPeOojrFOmyPC1du8cDWcJnZ0ixz5TfLBumo5s9DKnOpwSAeNITXqxmNJGAZl456xjj2Z7asTQ5KsCUdDvI6nDIRwINchNx10ClUdPJ6HVOVMW17y0mtvEraMSru73jRbGoPDmhnhVvanKDatvBJM9ja7kSM7YumJwoJOBzPHArnknhZ2nAhEi2jbq9GRo5PbTkDB3HCqx46hV0OKfNpbO21dW0kLy7M5uaNkLIk5O64IJUl8ZwdNCKYTuOppq6OdcuGuJbqR54Y4mubEMoRzIDu7wBJKrg9MaY6qq7KRWjim3RvtEoLY1xgHGezNbXhCtbu2ksGvHt3UK8CCBJAxG6mMgsxbVQOiBqe+l3YPIqe6jVoLOWSIEqHuJxGoKkqRuA50IIxu9VJ16LnJpX6813Gnhq8acE3bf1o+80jKmdWXI7xpWXeQoklqEAVedY6cNUYmt//ssvQP8AwVie7nTn646Xr3ksY7kW8mzJufKc4I4WEilc7u8d18Bc6ZIHV21THDSjx9uZfLFxnll78i9FLbx7wV4lLMWbpAZJ4njViK4RvJdW8zA/0qS25BXzeTsqFB2ySRA+pQx9dVdv8ibm2hMs2z7bcDKvtcqhsuwRcYUdbDrqP8ST1v7cyX86CyTXvyMkxvJdske7vyXNtEm/krkEP0sa7ueOOqutbC2vtu5NwFOzF5idoGzHPqUCsSPbOHSHGkPYXI+8W9toltrmz9taQzlVnVCsbbvSJZNThQG7a6Ps7kNdQ86Y9qzqZpDLJiGDpOwAJ1Q40UaDTSn6MdmNjLxE1Kd11nf5Itj8n9r25nKXFhmeZpnBilIDMFUhfbB0cINDnr1qltLlFtSC5khmudnxrHbC4aUwy7uC7Rhcc7neyumOOQBWDJyklhF0Lna9zzsV08EUMUcBkkCKhDY5vo+UekcDTtpMnWe5lE97K0sgG6itundUFmUMVVQxBY646z3YslJR1FZ1FBZmhtXlBd7RZHu2Xm4yTFGilFJ1xIylmO9jgCdNe05gllCjvOgHaeoV4ubpIwC7BQTgE9tUtq7M54BlkZWXVCDoPV29tLt7TuxFy25XloVm2fdgsVuE6RyRu6fWDp1V72RPPkwSoOgPLB03dQPOTjThwPZXnZ213DczOpEvUQM73/L08OPCtS6nEYyFLMxAVVGS7HRVAGpJrrb0aJScvxaXcafJTYnjt/Db49phxNP2bq/o04Y6TY07AT1V+hKU/BxyVNja4kwbiU85Ow/WPBR+yg6I78nrpsq+MbKw5ThsxsYHhA9y775NL/Y1S8ifc2y+Sw/ZrUXhA9y775NL/Y1S8ifc2y+Sw/ZrUiYv+Fb4L+dbf7ynqkXwrfBfzrb/AHlPVABWTyo2BFfWz28wO62oYeUjDVWU9RB9eoOhNa1FAH5vvLOa1na1uRiZNQ3vZU6nXz9Y6jms6+ZIFaVYQTg5KqB6zxx2mv0Fyx5Jw7Qh3JMq66xSr5Ubdo7QeteB9RHDNqwSWdybS73BLgFWU9GVTkAj9UnBG6aonC2aFKlLZe0tOArWzskokmjaSaRSUUY6I10weGg9Hnra2DzvNe2jByd0HiF6gfNqPNivNxY4uI5sjdRSpHDd0bB82uK9y3e+Ohvc3rvOuucdS41x2sM93aOSdyEpbayLx3WBGhHAjiO8Gp9kbUvLIEWVwUQ59pkG/Fk5PRB1Q5OejS3yUYLbO54b7N6AB+Bq3abVLRGYoBHrwbLAA4JIxjv4+uuJSi8iKU4SeyMH/aTtUH26VI8cGW2WRR6Q2R6qi2Jysuoo+bg2rCql2fHMx5y7F28s54sdKrK4xngMZ1076jlto38pEbzgGrI10vyiWLEveMkfKPapGRtMkdot4PyVUN1fG48ZN/Jz3Nc1viKIdAsHK43MeUAc4zS6dkxg5j3om/WjJX6uB9Vehtx4MpOOcJHtTqMb54brDqbXjwxTVKrRnk1YlGq5aMa/8Z2j8ZT/AMkX/wCusvbG0ZpE5u52nKVyDus0S6qQy+9GoIB9FYzWskutxI2D/pISqDuONW85NSRbMhXhEn8oJ9ZqE8RST7MTjr23lyfldNqE2nfSnsiIP1hAB66p/wCIbSkbJv72JOw3DMx/l3VXza1O8qpgEhewcM+YddVE2sjDeQM6hgpYDQcOOSDgZzkCqJVpS/FWI/em12UWLe1VSzas7HLOxLOxPEsx1NeJrwDO6rSEcQmDjuySBnu491Z7XcguhFKRuMp3d3IBPeePURx7KrxjxS5xwhl4din/AK+ojsqrZvqQ2LvPN2uTzX0NzEwIPtZDMjDBAHlYx3Z9NVksp4MGA5Rm0ifU6/UOs6EemrZsc3bOmMc2Q/ZvHQD1YJHm7auGRLeMb7k40yTkseGAP+Q//tSvbJEtpLKPoSMQo5yXdBVdSOAHEgZ17PPgV0TwW8jmZ12hdJg4/wAtE3FAf9Rh+uw4DqHeRip4NORIuhHf3e60flQQAhl7nlI0Ldie969ciuwVZCFs2X0qWzm9QoooqwvMDwge5d98ml/sapeRPubZfJYfs1qLwge5d98ml/sapeRPubZfJYfs1oAX/Ct8F/Otv95T1SL4Vvgv51t/vKatt7bt7SPnbmVIkzjLHieOAOLHQnABOhoA0Ky9s8orW13fGbiKLe8kOwBOOOBxx30tbU8J9j4pcS20yyTRJlYmV0YsxVE6LBWK7zLnHVXPr3dtlM9wTc3cpC7z43pHbgi50RB2DAAHmFL18QqVla7eiLIU9rwOn8qOWsUECNbslxNPpbojBlbtZiOEa8SfRXIbobzTRmHx6VzvXkrMFyx4KhPAqPJUY3QANCahs7IxSNBFuC8mG/PIqgLAh6kHb2DiT0j1Vs7SlSwtMRLlvJjXiXkbgT+sc6nzVnYnFOcoxjv0Xy+7h6l9Okkm2K0CsCRbs0oHG3l6FxHw0AP6QD8K+wzxyBkRtxz5SlcMD15U/wDXfWptCZMCC5ge5khiEksqlVZM6krqrHGPe9grze7DMixlJI7pGXejWc7su7gH2uVcMeI8oYHXVkcQl+fXWudherg4yd45PrdyMK2t3t41TG+m+d8gZ6BBzkceJHDOgqgIRFcJHCweOXy487wA4E+rh5q1pkeE4MkkJ6ku13k/hnj0PpFS+2bu81szIf8AUgIlU9+UO8PVTSqLW+vXVhWVGrFvK/gU9tzb8cigsFVTkgHDMOrIGAB19p06jUmzJM2anJGEOoOD0c/hXmO7g5sxLKE4jEmcjPEYbB+uvNtZOts0KMj5BCsDjRs5zx7e2pbrFDXZtpmR7FvS1uWebpnPErkY4aEd3XU2xJGniWSXDEMSumMY0z/WiC1dLQx7hLhWAwQdTnXjw189Tcn4CkCowIYZyD3kn00Sas2jk2rNriQQXUhu2iZ+iF3hgAZ8njp3n1VVMpFzLFIZHXcygBOerhu+c691WJoJPHBKqMU3N0nQa69pGeqpLqxkN0syBQFXdO8cZ49gPb19ldVvY6ml6e5BybUFRvSb8iFtDnKBsAg5Ger6zXzY/td1PF1N01/qf6/VU0CRRSySyTJvv1ZAAGnAZyToNa9pzcr78cEs79TBCQMd7YUCuN630O2cm0k3c88oLJpBGYx7YjZXzdeTwHAGrN3brJGOfCgKd4jOn8xA/wCVRy3Em9us8UTfqLm4l7uhH0R/EavWfJ+VyG5vd/3l2Q7D9yFegp/eJqqdaMFm+uuFy6nhaskk8repUjnJTMKokQ056ToRD93rc9yirmx7STPO20XPONfGLnKKe6FOKg6jeOK0LmzhhkAYNc3ZjZoud8klBkqg8lT14AzpxrMu+Vu8kF1uyRMh1jJykyE7rhW8kuuMgHBGvGlXUnVXYWT49XafHJcUPU6FOj4+4w8l+Uj2cjzW6MYt7/OWXv4z1yRDhnr06LgdR4Pl54U7Ibot1nuyVDHxePe3ARkb5YqA37PEdYFIu1bSORVuo5VikVcpP70qdcP+sh7OrqpetdpmFBeIm4kr7ssHAStoOct86knPDGuvYDUsPi5unaKu9M+PC+/ufrxJzpLazO/cm+Udvex85bvnBw6EbroetXU6qfqPVmtauI383i0sV9FlJI3QSY05yN2VHRx16NkZ4EDspp2h4TniupYDs6fEOrnnEEhXXppH79T3N3U7h8TGtT29Nz8SmpTcJWGbwge5d98ml/sapeRPubZfJYfs1rN5UbUiudiXU8LB45LSUq38Dcewg5BHUQa0uRPubZfJYfs1pkrF/wAK3wX862/3lZvhPT/vHZxcZTdnC9gfEZ82SoOPNWl4Vvgv51t/vK3eWPJtb635ouY5FYPFKoyY3XyTjrGpBHWCeHGq6sNuDjxRKLs0zjpkVbCaze1kN9LOjG4CbyygTK4POe8CouNw4A0PWa3dqQxPE4nCmLBLb3AAak92O2pp9i7Xj6Jsop8e/iuFQHv3ZACPNk1Z2fyEu7t1/wAQ5uG2BBNvE++8hGuJHwAE4aLx7tDWVUw+JrzjtpRUd6f6Go1KcE7O9xTsuRV5LYw3W4Z1cF0QNuXUKEnmzHJnEgKYbdbtAFUre8YyI0qm68XJIwCk8ROh52A43iMeUOzNfopEAAAAAGgA6qw+UfJG0vcGaPEg8maM7kqeZxr6Dkd1aFXCxmsuuXl53KI1Wjg0MUlwyc1iV0lP+Z5wZEbMSySxt0+B3d0jHZ11q7EKTX8zjdC2y8xFGMDGP0jbvUPeg8Meat3lF4NrtG34wt4BwdW8XulHDRxhJMDPHGeyk+YlZ150K0yEFUu1NrcDHkgSDCSDOeOhpKtQnZrut669dm2ZdCa665m9yo24YVKRIsku4ZGVtVVF1Zm8/ADrNZ+17ewiSOZleBpcYa3LIRkb3BSBgduDWdfW9vuzmcXEFxLvYklLBTvDCrvx5jMY0GvVXna9q8sEW6FmW3tAN6Ng4MhKIwGMnRVJzVFKjGOyk2s83pf4auTlJu5s3Gz5OcMIvY5WC55q5jSQ47cjDY9FUZeT7kdKws5O+GVofVpp66z4kdWvp5NJVtjv/svMN5QP3U3FrT2hC9rb2dxGAZI41hZCcBxIBp6Hwa69qLUVJXflna+qt3epzJ5tdeZWOyABrY3if+ncB/6vUX+Gjqj2t/8AiNWttSPBHFam7KTSlpZZi+7jAJAU5G6rMAoA6ge2p9obdd7KzuIy2WnjEipoWxvB16uLDge6pKpVsmtG7av5vrZkXTp53WngUY9kg/8Al9pv+/Kif0YVNHyeLHP+Gjzz3TN/wrvCtCw2lLJtLdZJYk8WJ5tyNSH8rAJHA4zx0qpy6RhcWsiZ3kWR1x1mMLJj+IBhUFVqSqKm3a6vq+/g0d+3BR2kvZcixabElyQviMDDjzMPOOPOXI/pU8OyoJpHjluZbh0xvxtIVUZ/YQKP61kcj5t6+eYZ3blJXGf2ZcL/AMNQW1rcvJLc28S70dzK2+XwZF0Ux7oGowo1J48KJwntSTnbJZ5LN9+u7iSTVk7Gpa7QlUT+KWkCJA7K6lt133dTgKuNerJOagurjxp+bLNzF9BmIMdI5Y9cdg4ZPaRXm/vDDPJLHPBCLiNDIkhLyI4BGkaZJbBx2ZzXwOqW8KR25CQNvJPdPzC72p3gM77Alj0cYrqha0ks+Plxe9StpuON7uurHifaTXEVum6/j8Eq7w3T0d04dmbG6EZdTrrVtZYlkuIrdfGllbeMIA5qNtd8tIeiAdDgZIxXqy2fcX56Cz3q54IDbWgwcHLtrJgjqz10/wCxfBjlQL6UMg/8tbgxQjuYjDyendHdV8MNKStay9tb9zy3aeJB1Ejn2y9lS3UgjiRbt49AidCzt8YHSb37DOcak66Vu8oeSr7Omt7u4lNwr5ilcqAsDt+jMY95GdUJPXgk9LFdksbKOFFjiRY0XgqKFA8wGlG0LGOeJ4pUDxupVlPAg/8AXGnP4sNhxe9a8uHVyn7rvc49t+wae3kjUgMQCpPDKkMM92RRtvak9xNHd3awQJbI+BGxdm3hht5iB0dAQoHGtebkHtCBty1mgmg954wXWRB1KWRSHA7Tg1pbH8HRZ1l2hMJypysCLuQKRwLAktIR1b2ndWfQweIgnSutl795fOrTdpbynsqxeHktMsi7rNazybv6ok5yRR3dFhpTlyJ9zbL5LD9mtQ+ED3Lvvk0v9jVNyJ9zbL5LD9mtbIoL/hW+C/nW3+8p6pL8KGz7iWKze3hadoL6KdkVlUlYw5OCxA4kD0189l20PiW4+mi/GgB1opK9l20PiW4+mi/Gj2XbQ+Jbj6aL8aAHWikr2XbQ+Jbj6aL8aPZdtD4luPpovxoAdaqbS2ZDcJuTxRyp+rIoYeojj30q+y7aHxLcfTRfjR7LtofEtx9NF+NAEV34L7XXxWWe1znoxvvx69scm8voGKU9peCe6DbyeJz413t17SXPnjJUnz4px9l20PiW4+mi/Gj2XbQ+Jbj6aL8ag6cXnYkpNHM73kptKNGV7e+3TxCvBeKR+62GI7jrVG/lutxROr4VgwE9jPHgrqP0ZPD1V1r2XbQ+Jbj6aL8aPZbtD4luPpovxqp4WD6XIl92RxuLlCOeeVjZNIyhTmSVcBc6BXj076gG1Itwpu2+6ZuewLrg2QdPa9F04V2luVV+eOxJz55ofxrz7Jr34jm+lh/GofxILTqx37rORycplMwnxaiQIUB8YZuiTvcFj7al/wAamuCjIkLlCShSC5mIyMHHQUcK6wvKi+HDYc4/96H8a9+y3aHxLcfTRfjXFgaSd7HfvSOWWOyNovuiK3uUCghdyyjgADHJCtM+QCdTgca07fwcbRmBEkW6CeFzdkjv6Fuu7r2Z1roHsu2h8S3H00X40ey7aHxLcfTRfjVyw8Fu68iDqSMPZXgmdPLu1iBGq2kKxH6R99z9VM+y/B5s6Fg/i4lk09snJmbTgemSAfMBVT2XbQ+Jbj6aL8aPZdtD4luPpovxqyMIx0RFtsdAK+0ley7aHxLcfTRfjR7LtofEtx9NF+NSODrRSV7LtofEtx9NF+NHsu2h8S3H00X40AOtFJXsu2h8S3H00X40ey7aHxLcfTRfjQBreED3Lvvk0v8AY1S8ifc2y+Sw/ZrSnyj25tG5tLi3Gx51M0TxhjNEQN5SuePVmnLkravFY2sUg3XjgjRhocMqKGGmmhFAGpRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAf//Z';

                        $(win.document.body).prepend('<div> <img src= ' + gambar + ' width="100" height="100"> </img> </div>')
                        // $(win.document.body).find('thead').prepend('<div class="header-print">' + 'testing lur' + '</div>');
                    }
                }
                // {
                //     extend: 'print',
                //     action: function() {
                //         printJS({
                //             printable: 'dataTable',
                //             type: 'html',
                //             style: 'table,td,th{border: 1px solid black;} table {width: 100%; border-collapse: collapse;}'
                //             // targetStyles: [''],
                //             // header: 'PrintJS - Print Form With Customized Header'
                //             // style: '@media print { table, td, th { border: 1 px solid black; } table { width: 100 % ; border - collapse: collapse; } } ',
                //         })
                //     }
                // action: function() {
                //     printJS({
                //         printable: 'dataTable2',
                //         type: 'html',
                //         // style: '#dataTable{color:blue;}'
                //         targetStyles: ['*'],
                //         // header: 'PrintJS - Print Form With Customized Header'
                //         // style: '@media print { table, td, th { border: 1 px solid black; } table { width: 100 % ; border - collapse: collapse; } } ',
                //     })
                // }
                // }
            ]
        });
    });
</script>
</body>

</html>