  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url('assets/'); ?>vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url('assets/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url('assets/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url('assets/'); ?>js/sb-admin-2.min.js"></script>

  <script>
    $('#btn_login_details').click(function() {
      var nama = $('#name_home-tab').val();
      var email = $('#email_home-tab').val();
      var gender = $('#gender option').filter(':selected').val();
      var address = $('#address_home-tab').val();
      var password1 = $('#password1').val();
      var password2 = $('#password2').val();
      $.ajax({
        url: "<?= base_url('auth/registration_login_detail'); ?>",
        type: "post",
        data: {
          name: nama,
          email: email,
          gender: gender,
          address: address,
          password1: password1,
          password2: password2
        },
        success: function(data) {
          data = jQuery.parseJSON(data);
          if (data.errors) {
            $('#error_name').text(data.errors.name);
            $('#error_email').text(data.errors.email);
            $("#error_address").text(data.errors.address);
            $('#error_gender').text(data.errors.gender);
            $('#error_password1').text(data.errors.password1);
            $('#error_password2').text(data.errors.password2);
          } else {
            $('#error_name').text('');
            $('#error_email').text('');
            $("#error_address").text('');
            $('#error_gender').text('');
            $('#error_password1').text('');
            $('#error_password2').text('');

            $('#profile-tab').removeClass('disabled');
            $("#home-tab").addClass('disabled');
            $("#contact-tab").addClass('disabled');
            var firstTabEl = document.querySelector('#profile-tab')
            var firstTab = new bootstrap.Tab(firstTabEl)

            firstTab.show()
          }
          // data = jQuery.parseJSON(data);
          // console.log('data', data)
          // document.location.href = "<?= base_url('admin/registration_login_detail'); ?>";
        }
      })

    });



    // button pendidikan
    $('#btn_pendidikan_previous').click(function() {
      $('#profile-tab').removeClass('disabled');
      $("#home-tab").addClass('disabled');
      $("#contact-tab").addClass('disabled');
      var firstTabEl = document.querySelector('#profile-tab')
      var firstTab = new bootstrap.Tab(firstTabEl)

      firstTab.show()
    })

    $('#btn_pendidikan_submit').click(function() {

      var asal_sekolah = $('#asal_sekolah').val();
      var nilai = $('#nilai').val();

      var birthday = $('#birthday').val();
      var agama = $('#agama').val();
      var nohp = $('#nohp').val();

      var nama = $('#name_home-tab').val();
      var email = $('#email_home-tab').val();
      var gender = $('#gender option').filter(':selected').val();
      var address = $('#address_home-tab').val();
      var password1 = $('#password1').val();

      var myFormData = new FormData();
      myFormData.append('namafile', $("#file_kirim").prop('files')[0]);
      myFormData.append('agama', agama);
      myFormData.append('birthday', birthday);
      myFormData.append('nohp', nohp);
      myFormData.append('asal_sekolah', asal_sekolah);
      myFormData.append('nilai', nilai);
      myFormData.append('nama', nama);
      myFormData.append('email', email);
      myFormData.append('gender', gender);
      myFormData.append('address', address);
      myFormData.append('password1', password1);
      $.ajax({
        url: "<?= base_url('auth/registration_pendidikan_detail'); ?>",
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        dataType: 'json',
        data: myFormData,

        success: function(data) {
          console.log(data)
          // data = jQuery.parseJSON(data);
          if (data.success == '') {
            $('#error_asal_sekolah').text(data.errors.asal_sekolah);
            $('#error_nilai').text(data.errors.nilai);
            $('#error_photo').text(data.errors.namafile);
          } else {
            $('#error_asal_sekolah').text(' ');
            $('#error_nilai').text(' ');
            $('#error_photo').text(' ');
            document.location.href = "<?= base_url('auth'); ?>";
          }
        },
        error: function(xhr, a, b) {
          console.log(xhr)
          // alert(xhr);
        }
      });

    });





    // end button pendidikan

    // button detail personal
    $('#btn_profile_next').click(function() {
      // alert();
      var birthday = $('#birthday').val();
      var agama = $('#agama').val();
      var nohp = $('#nohp').val();

      var myFormData = new FormData();
      myFormData.append('namafile', $("#file_kirim").prop('files')[0]);
      myFormData.append('agama', agama);
      myFormData.append('birthday', birthday);
      myFormData.append('nohp', nohp);
      $.ajax({
        url: "<?= base_url('auth/registration_personal_detail'); ?>",
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        dataType: 'json',
        data: myFormData,

        success: function(data) {
          // data = jQuery.parseJSON(data);
          if (data.success == '') {
            $('#error_birthday').text(data.errors.birthday);
            $('#error_agama').text(data.errors.agama);
            $("#error_hp").text(data.errors.nohp);
            $('#error_photo').text(data.errors.namafile);
          } else {
            $('#error_birthday').text(' ');
            $('#error_agama').text(' ');
            $("#error_hp").text(' ');
            $('#error_photo').text(' ');

            $('#profile-tab').addClass('disabled');
            $("#home-tab").addClass('disabled');
            $("#contact-tab").removeClass('disabled');
            var firstTabEl = document.querySelector('#contact-tab')
            var firstTab = new bootstrap.Tab(firstTabEl)

            firstTab.show()
          }
          // data = jQuery.parseJSON(data);
          // console.log('data', data)
          // document.location.href = "<?= base_url('admin/registration_login_detail'); ?>";
        }
      })
    });

    $('#btn_profile_previous').click(function() {
      $('#profile-tab').addClass('disabled');
      $("#home-tab").removeClass('disabled');
      $("#contact-tab").addClass('disabled');
      var firstTabEl = document.querySelector('#home-tab')
      var firstTab = new bootstrap.Tab(firstTabEl)

      firstTab.show()
    });
    // end button detail personal

    $(document).on("click", ".browse", function() {

      var file = $(this).parents().find(".file");
      file.trigger("click");
    });
    $('input[type="file"]').change(function(e) {
      var fileName = e.target.files[0].name;
      $("#file").val(fileName);

      var reader = new FileReader();
      reader.onload = function(e) {
        // get loaded data and render thumbnail.
        document.getElementById("preview").src = e.target.result;
      };
      // // read the image file as a data URL.
      reader.readAsDataURL(this.files[0]);
    });
  </script>

  </body>

  </html>