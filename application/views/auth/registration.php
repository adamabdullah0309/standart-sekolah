<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5 col-lg-7 mx-auto">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                        </div>
                        <!-- <form class="user" method="post" action="<?= base_url('auth/registration'); ?>"> -->
                        <!-- <form class="user" method="post" action="<?= base_url('auth/registration_login_detail'); ?>"> -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Detail akun</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link disabled" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Detail personal</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link disabled" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Detail pendidikan</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <br>
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="name" id="name_home-tab" class="form-control" placeholder="Nama" />
                                    <span id="error_name" class="text-danger"></span>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="email" id="email_home-tab" class="form-control" placeholder="Email" />
                                    <span id="error_email" class="text-danger"></span>
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <select class="form-control" name="gender" id="gender">
                                        <option selected value="">Select gender</option>
                                        <option value="l">Man</option>
                                        <option value="p">Woman</option>
                                    </select>
                                    <span id="error_gender" class="text-danger"></span>
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input type="text" name="address" id="address_home-tab" class="form-control" placeholder="Alamat" />
                                    <span id="error_address" class="text-danger"></span>
                                </div>

                                <label>Enter password</label>
                                <div class="form-group row">

                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" class="form-control form-control-user" id="password1" name="password1" placeholder="Password">
                                        <span id="error_password1" class="text-danger"></span>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control form-control-user" id="password2" name="password2" placeholder="Repeat Password">
                                        <span id="error_password2" class="text-danger"></span>
                                    </div>
                                </div>
                                <br />
                                <div align="center">
                                    <button type="button" name="btn_login_details" id="btn_login_details" class="btn btn-primary btn-lg">Next</button>
                                </div>

                            </div>

                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <br>
                                <div class="form-group">
                                    <label>Tanggal lahir</label> <br>
                                    <input type="date" id="birthday" name="birthday">
                                    <br>
                                    <span id="error_birthday" class="text-danger"></span>
                                </div>
                                <div class="form-group">
                                    <label>Agama</label>
                                    <select class="form-control" name="agama" id="agama">
                                        <option selected value="">Pilih agama</option>
                                        <option value="1">Budha</option>
                                        <option value="2">Hindu</option>
                                        <option value="3">Islam</option>
                                        <option value="4">Katolik</option>
                                        <option value="5">Khong Hu Chu</option>
                                        <option value="6">Kristen</option>
                                        <option value="7">Lainnya</option>
                                    </select>
                                    <span id="error_agama" class="text-danger"></span>
                                </div>
                                <div class="form-group">
                                    <label>Nomor HP</label>
                                    <input class="form-control" type="number" placeholder="Nomor HP" id="nohp" name="nohp" aria-label="default input example">
                                    <span id="error_hp" class="text-danger"></span>
                                </div>
                                <label>Photo</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="https://placehold.it/80x80" id="preview" class="img-thumbnail">
                                        </div>
                                        <div class="col-lg-6">
                                            <form method="post" id="image-form">
                                                <input type="file" name="img[]" id="file_kirim" style=" visibility: hidden; position: absolute;" class="file" accept="image/*">
                                                <div class="input-group my-3">
                                                    <input type="text" class="form-control" disabled placeholder="Upload File" id="file">
                                                    <div class="input-group-append">
                                                        <button type="button" class="browse btn btn-primary">Browse...</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <span id="error_photo" class="text-danger"></span>

                                </div>
                                <br>
                                <br>
                                <div align="center">
                                    <button type="button" name="btn_login_details" id="btn_profile_previous" class="btn btn-warning btn-lg">Previous</button>
                                    <button type="button" name="btn_login_details" id="btn_profile_next" class="btn btn-primary btn-lg">Next</button>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <br>
                                <div class="form-group">
                                    <label>Asal sekolah</label>
                                    <input type="text" name="asal_sekolah" id="asal_sekolah" class="form-control" />
                                    <span id="error_asal_sekolah" class="text-danger"></span>
                                </div>
                                <div class="form-group">
                                    <label>Nilai</label>
                                    <input type="text" name="nilai" id="nilai" class="form-control" />
                                    <span id="error_nilai" class="text-danger"></span>
                                </div>
                                <br>
                                <br>
                                <div align="center">
                                    <button type="button" name="btn_login_details" id="btn_pendidikan_previous" class="btn btn-warning btn-lg">Previous</button>
                                    <button type="button" name="btn_login_details" id="btn_pendidikan_submit" class="btn btn-primary btn-lg">Submit</button>
                                </div>
                            </div>
                        </div>
                        <!-- <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active_tab1" style="border:1px solid #ccc" id="list_login_details">Login Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link inactive_tab1" id="list_personal_details" style="border:1px solid #ccc">Personal Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link inactive_tab1" id="list_contact_details" style="border:1px solid #ccc">Contact Details</a>
                                </li>
                            </ul>
                            <div class="tab-content" style="margin-top:16px;">
                                <div class="tab-pane active" id="login_details">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Login Details</div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>Enter Email Address</label>
                                                <input type="text" name="email" id="email" class="form-control" />
                                                <span id="error_email" class="text-danger"></span>
                                            </div>
                                            <div class="form-group">
                                                <label>Enter Password</label>
                                                <input type="password" name="password" id="password" class="form-control" />
                                                <span id="error_password" class="text-danger"></span>
                                            </div>
                                            <br />
                                            <div align="center">
                                                <button type="button" name="btn_login_details" id="btn_login_details" class="btn btn-info btn-lg">Next</button>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="personal_details">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Fill Personal Details</div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>Enter First Name</label>
                                                <input type="text" name="first_name" id="first_name" class="form-control" />
                                                <span id="error_first_name" class="text-danger"></span>
                                            </div>
                                            <div class="form-group">
                                                <label>Enter Last Name</label>
                                                <input type="text" name="last_name" id="last_name" class="form-control" />
                                                <span id="error_last_name" class="text-danger"></span>
                                            </div>
                                            <div class="form-group">
                                                <label>Gender</label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="gender" value="male" checked> Male
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="gender" value="female"> Female
                                                </label>
                                            </div>
                                            <br />
                                            <div align="center">
                                                <button type="button" name="previous_btn_personal_details" id="previous_btn_personal_details" class="btn btn-default btn-lg">Previous</button>
                                                <button type="button" name="btn_personal_details" id="btn_personal_details" class="btn btn-info btn-lg">Next</button>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact_details">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Fill Contact Details</div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>Enter Address</label>
                                                <textarea name="address" id="address" class="form-control"></textarea>
                                                <span id="error_address" class="text-danger"></span>
                                            </div>
                                            <div class="form-group">
                                                <label>Enter Mobile No.</label>
                                                <input type="text" name="mobile_no" id="mobile_no" class="form-control" />
                                                <span id="error_mobile_no" class="text-danger"></span>
                                            </div>
                                            <br />
                                            <div align="center">
                                                <button type="button" name="previous_btn_contact_details" id="previous_btn_contact_details" class="btn btn-default btn-lg">Previous</button>
                                                <button type="button" name="btn_contact_details" id="btn_contact_details" class="btn btn-success btn-lg">Register</button>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        <!-- <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="name" placeholder="Full Name" name="name" value="<?= set_value('name'); ?>">
                                <?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="email" placeholder="Email Address" name="email" value="<?= set_value('email'); ?>">
                                <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input type="password" class="form-control form-control-user" id="password1" name="password1" placeholder="Password">
                                    <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control form-control-user" id="password2" name="password2" placeholder="Repeat Password">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                Register Account
                            </button> -->

                        <!-- </form> -->
                        <hr>
                        <div class="text-center">
                            <a class="small" href="<?= base_url('auth/forgotpassword') ?>">Forgot Password?</a>
                        </div>
                        <div class="text-center">
                            <a class="small" href="<?= base_url('auth'); ?>">Already have an account? Login!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>