<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $title; ?></h6>
        </div>
        <br>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Photo</th>
                            <!-- <th>Photo2</th> -->
                            <th>Jenis Kelamin</th>
                            <th>Alamat</th>
                            <th>Tanggal Lahir</th>
                            <th>Nomor Hp</th>
                        </tr>
                    </thead>
                    <!-- <tfoot>
                        <tr>
                            <th>Nomor</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Photo</th>
                            <th>Photo2</th>
                            <th>Jenis Kelamin</th>
                            <th>Alamat</th>
                            <th>Tanggal Lahir</th>
                            <th>Nomor Hp</th>
                        </tr>
                    </tfoot> -->
                    <tbody>
                        <?php $i = 1;
                        foreach ($list as $l) : ?>
                            <tr>
                                <th scope="row"><?= $i; ?></th>
                                <td><?= $l['name']; ?></td>
                                <td><?= $l['email']; ?></td>
                                <!-- <td><img src="<?= base_url('assets/img/profile/') . $l['image']; ?>" width="80" height="80"></td> -->
                                <?php
                                $path = base_url('assets/img/profile/'). $l['image'];
                                $type = pathinfo($path, PATHINFO_EXTENSION);
                                $data = file_get_contents($path);
                                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                ?>
                                <td><img src="<?= $base64; ?>" width="80" height="80"></td>
                                <!-- <td><?= base_url('assets/img/profile/') . $l['image']; ?></td> -->
                                <td><?php if ($l['jenis_kelamin'] == 'l') {
                                        echo 'Laki - laki';
                                    } else if ($l['jenis_kelamin'] == 'p') {
                                        echo 'Perempuan';
                                    } ?></td>
                                <td><?= $l['alamat']; ?></td>
                                <td><?= $l['tanggal_lahir']; ?></td>
                                <td><?= $l['nomor_hp']; ?></td>
                                <!-- <td> -->
                                <!-- <div class="form-check">
                                        <input class="form-check-input" type="checkbox" <?= check_access($role['id'], $m['id']); ?> data-role="<?= $role['id']; ?>" data-menu="<?= $m['id']; ?>">

                                    </div> -->
                                <!-- </td> -->
                            </tr>
                        <?php $i++;
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->