<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		
	}

	public function index()
	{
		$this->goToDefaultPage();
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if($this->form_validation->run() == false)
		{
			$data['title'] = 'User Login';

			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/login');
			$this->load->view('templates/auth_footer');
		}
		else
		{
			//validasinya success
			$this->_login();
		}
		
	}

	private function _login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$user = $this->db->get_where('user', ['email' => $email])->row_array();
		
		//jika user nya ada
		if($user){
			//jika user nya aktif
			if($user['is_active'] == 1)
			{
				//cek password
				if(password_verify(($password), $user['password']))
				{
					$data = [
						'email' => $user['email'],
						'role_id' => $user['role_id']
					];
					$this->session->set_userdata($data);

					if($user['role_id'] == 1)
					{
						redirect('admin');
					}
					else
					{
						redirect('user');
					}
				}
				else
				{
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
					Wrong password !
					</div>');
					redirect('auth');
				}
			}
			else
			{
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
				This email is not active
				</div>');
				redirect('auth');
			}

		}
		else
		{
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
			Email is not registred
			</div>');
			redirect('auth');
		}
	}

	public function registration_login_detail()
	{
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
			'is_unique' => 'This email has already registered!'
		]);
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');

		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
			'matches' => 'Password dont match !',
			'min_length' => 'Password too short !'
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|matches[password1]');

		if ( $this->form_validation->run() == false) 
		{
			$errors = validation_errors();
			echo json_encode(
				[
					'errors' => [
						'name' => form_error('name', ' ',' '),
						'email' => form_error('email',' ', ' '),
						'address' => form_error('address', ' ', ' '),
						'gender' => form_error('gender', ' ', ' '),
						'password1' => form_error('password1', ' ', ' '),
						'password2' => form_error('password2', ' ', ' '),
						
					],
					'success' => []
				]
				
			);
		}
		else{
			echo json_encode(['success' => 'Record complete.']);
		}
	}

	public function registration_personal_detail()
	{
		
		$this->form_validation->set_rules('agama', 'Agama', 'required|trim');
		$this->form_validation->set_rules('birthday', 'Tanggal lahir', 'required');
		$this->form_validation->set_rules('nohp', 'Nomor HP.', 'required');

		$nama_file = '';
		$config=[];
		if (isset($_FILES['namafile']['name'])) {
			$nama_file = $_FILES['namafile']['name'];
		}
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '2048';
		$config['upload_path'] = './assets/img/profile/';
		$config['file_name']	 = $nama_file;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->form_validation->run() == false 
		|| $this->upload->do_upload('namafile') === FALSE
		) {
			echo json_encode(
				[
					'errors' => [
						'agama' => form_error('agama', ' ', ' '),
						'namafile' => $this->upload->display_errors(' ',' '),
						'birthday' => form_error('birthday' , ' ', ' '),
						'nohp' => form_error('nohp', ' ', ' ')
					],
					'success' => ''
				]

			);
		}
		else
		{
			$new_image = $this->upload->data('file_name');
			unlink(FCPATH . 'assets/img/profile/' . $new_image);
			echo json_encode(
				[
					'errors' => [
					],
					'success' => 'Record complete.'
				]

			);
			// $new_image = $this->upload->data('file_name');
			// $this->db->set('image', $new_image);
		}
	}

	public function registration_pendidikan_detail()
	{
		$this->form_validation->set_rules('asal_sekolah', 'Asal sekolah', 'required|trim');
		$this->form_validation->set_rules('nilai', 'Nilai', 'required');

		$nama_file = '';
		$config = [];
		if (isset($_FILES['namafile']['name'])) {
			$nama_file = $_FILES['namafile']['name'];
		}
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '2048';
		$config['upload_path'] = './assets/img/profile/';
		$config['file_name']	 = $nama_file;
		
		$this->load->library('upload', $config);

		if ($this->form_validation->run() == false
			|| $this->upload->do_upload('namafile') === FALSE
		) {
			echo json_encode(
				[
					'errors' => [
						'asal_sekolah' => form_error('asal_sekolah', ' ', ' '),
						// 'namafile' => $this->upload->display_errors(' ', ' '),
						'nilai' => form_error('nilai', ' ', ' ')
					],
					'success' => ''
				]

			);
		} else {
			$email = htmlspecialchars($this->input->post('email', true));
			$new_image = $this->upload->data('file_name');
			$data =
			[
				'name' => htmlspecialchars($this->input->post('nama', true)),
				'email' => htmlspecialchars($email),
				'image' => $new_image,
				'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
				'role_id' => 2,
				'is_active' => 0,
				'date_created' => time(),
				'jenis_kelamin' => $this->input->post('gender'),
				'alamat' => $this->input->post('address'),
				'tanggal_lahir' => $this->input->post('birthday'),
				'm_agama_id' => $this->input->post('agama'),
				'nomor_hp' => $this->input->post('nohp')
			];

			
			//siapkan token
			$token = base64_encode(random_bytes(32));
			$user_token = [
				'email' => $email,
				'token' => $token,
				'date_created' => time()
			];

			$this->db->insert('user', $data);
			$this->db->insert('user_token', $user_token);


			$this->_sendEmail($token, 'verify');

			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
			Your account has been created. Please activate your account
			</div>');

			echo json_encode(
				[
					'errors' => [],
					'success' => 'Record complete.'
				]

			);
		}
	}

	


	public function registration()
	{
		$this->goToDefaultPage();
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]',[
			'is_unique' => 'This email has already registered!'
		]);

		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]',[
			'matches' => 'Password dont match !',
			'min_length' => 'Password too short !'
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|matches[password1]');
		$email = htmlspecialchars($this->input->post('email', true));
		if( $this->form_validation->run() == false)
		{
			$data['title'] = 'User Registration';
	
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/registration');
			$this->load->view('templates/auth_footer');
		}
		else{

			$data =
			[
				'name' => htmlspecialchars($this->input->post('name', true)),
				'email' => htmlspecialchars($email),
				'image' => 'default.jpg',
				'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
				'role_id' => 2,
				'is_active' => 0,
				'date_created' => time()
			];


			

			//siapkan token
			$token = base64_encode(random_bytes(32));
			$user_token = [
				'email' => $email,
				'token' => $token,
				'data_created' => time()
			];

			$this->db->insert('user',$data);
			$this->db->insert('user_token',$user_token);


			$this->_sendEmail($token, 'verify');

			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
			Your account has been created. Please activate your account
			</div>');
			redirect('auth');
		}
	}

	private function _sendEmail($token, $type)
	{
		$config = [
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_user' => 'pizza.podang@gmail.com',
			'smtp_pass' => 'MIislamiyah03madiun',
			'smpt_port' => 587,
			// 'smtp_crypto' => 'ssl',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'newline' => "\r\n",
			'wordwrap' => true
		]; 
		
		$this->load->library('email', $config);
		$this->email->initialize($config); 

		$this->email->from('pizza.podang@gmail.com','Sekolah');
		$this->email->to($this->input->post('email'));

		if($type == 'verify')
		{

			$this->email->subject('Account Verification');
			$this->email->message('Click this link to verify your account : <a href="' . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Active</a> ');
		}
		else if($type == 'forgot')
		{
			$this->email->subject('Reset Password');
			$this->email->message('Click this link to reset your password : <a href="' . base_url() . 'auth/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Reset password</a> ');
		}

		if($this->email->send())
		{
			return true;
		}
		else
		{
			echo $this->email->print_debugger();
			die;
		}
	}

	public function verify()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');
		
		$user = $this->db->get_where('user', ['email' => $email])->row_array();
		
		if($user)
		{
			$user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();
			if($user_token)
			{
				if(time() - $user_token['date_created'] < (60*60*24))
				{
					$this->db->set('is_active', 1);
					$this->db->where('email', $email);
					$this->db->update('user');

					$this->db->delete('user_token', ['email' => $email]);
					$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
					'.$email.' has been activated ! Please login
					</div>');
					redirect('auth');
				}
				else
				{
					$this->db->delete('user',['email' => $email]);
					$this->db->delete('user_token', ['email' => $email]);
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
					Account activation failed ! Token expired
					</div>');
					redirect('auth');
				}
			}
			else
			{
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
				Account activation failed ! Wrong token
				</div>');
				redirect('auth');
			}
		}
		else
		{
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
			Account activation failed ! Wrong email
			</div>');
			redirect('auth');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('role_id');
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
			You have been logged out !
			</div>');
		redirect('auth');
		# code...
	}

	public function blocked()
	{
		$this->load->view('auth/blocked');
	}

	public function goToDefaultPage()
	{
		if ($this->session->userdata('role_id') == 1) {
			redirect('admin');
		} else if ($this->session->userdata('role_id') == 2) {
			redirect('user');
		} else {
			// jika ada role_id yg lain maka tambahkan disini
		}
	}

	public function forgotPassword()
	{
		$data['title'] = 'User Login';

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

		if($this->form_validation->run() == false)
		{
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/forgot-password');
			$this->load->view('templates/auth_footer');
		}
		else
		{
			$email = $this->input->post('email');
			$user = $this->db->get_where('user', ['email' => $email, 'is_active' => 1])->row_array();

			if($user)
			{
				$token = base64_encode(random_bytes(32));
				$user_token = [
					'email' => $email,
					'token' => $token,
					'date_created' => time()
				];
				$this->db->insert('user_token', $user_token);
				$this->_sendEmail($token, 'forgot');
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
				Please check your email to reset your password
				</div>');
				redirect('auth/forgotpassword');
			}
			else
			{
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
				Email is not registered or activated !
				</div>');
				redirect('auth');
			}
		}

		
	}

	public function resetpassword()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');

		$user = $this->db->get_where('user', ['email'=> $email])->row_array();

		if($user)
		{
			$user_token = $this->db->get_where('user_token', ['token' =>$token])->row_array();
			if($user_token)
			{
				$this->session->set_userdata('reset_email', $email);

				$this->changePassword();
			}
			else
			{
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
				Reset password failed ! Wrong Token
				</div>');
				redirect('auth');
			}
		}
		else
		{
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
				Reset password failed ! Wrong Email
				</div>');
			redirect('auth');
		}
	}

	public function changePassword()
	{
		if(!$this->session->userdata('reset_email'))
		{
			redirect('auth');
		}
		$data['title'] = 'User Login';

		$this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[3]|matches[password2]');
		$this->form_validation->set_rules('password2', 'Repeat Password', 'trim|required|min_length[3]|matches[password1]');

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/change-password');
			$this->load->view('templates/auth_footer');
		}
		else
		{
			$password = password_hash($this->input->post('password1'), PASSWORD_DEFAULT);
			$email = $this->session->userdata('reset_email');

			$this->db->set('password', $password);
			$this->db->where('email', $email);
			$this->db->update('user');

			$this->session->unset_userdata('reset_email');

			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
				Password has been changed! Please login.
				</div>');
			redirect('auth');
		}
	}
}
