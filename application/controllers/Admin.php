<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        // echo "selamat datang ". $data['user']['name'];

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/index', $data);
        $this->load->view('templates/footer');
    }
    public function role()
    {
        $data['title'] = 'Role';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['role'] = $this->db->get('user_role')->result_array();
        // echo "selamat datang ". $data['user']['name'];

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role', $data);
        $this->load->view('templates/footer');
    }

    public function roleAccess($role_id)
    {
        $data['title'] = 'Role';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['role'] = $this->db->get_where('user_role',['id' => $role_id])->row_array();
        $this->db->where('id !=',1);
        $data['menu'] = $this->db->get('user_menu')->result_array();
        // echo "selamat datang ". $data['user']['name'];

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role-access', $data);
        $this->load->view('templates/footer');
    }

    public function changeaccess()
    {
        $menu_id = $this->input->post('menuId');
        $role_id = $this->input->post('roleId');

        $data = [
            'role_id' => $role_id,
            'menu_id' => $menu_id 
        ];
        $result = $this->db->get_where('user_access_menu',$data);

        if($result->num_rows() < 1)
        {
            $this->db->insert('user_access_menu',$data);
        }
        else
        {
            $this->db->delete('user_access_menu', $data);
        }
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
			Access changed !
			</div>');
    }

    public function cetak_excel_list_siswa()
    {
        $data['coba'] = 'coba';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['list'] = $this->db->get_where('user', ['is_active', 1])->result_array();
        $print_view = $this->load->view('templates/print/list_siswa_excel', $data, TRUE);
        $print_file = fopen(('assets/').'print/list_siswa_excel.xls', 'w');
        fwrite($print_file, $print_view);
    }


    public function listsiswa()
    {
        $data['image'] = 'default.jpg';
        $data['title'] = 'List Siswa';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['list'] = $this->db->get_where('user', ['is_active',1])->result_array();
        // var_dump($data['list']);
        // die;
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/list-siswa', $data);
        $this->load->view('templates/footer');

        // $this->load->view('templates/template_print_list_siswa', $data);
    }
}
